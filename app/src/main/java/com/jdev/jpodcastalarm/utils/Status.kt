package com.jdev.jpodcastalarm.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}